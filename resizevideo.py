import os
from moviepy.editor import VideoFileClip

def resize_video(input_video, output_video, target_resolution):
    clip = VideoFileClip(input_video)
    resized_clip = clip.resize(target_resolution)
    
    # Calcular el bitrate objetivo para un tamaño de archivo deseado (aproximadamente 100 MB)
    target_size_mb = 100
    target_size_bytes = target_size_mb * 1024 * 1024
    target_bitrate = int(target_size_bytes * 8 / (resized_clip.duration * target_resolution[0] * target_resolution[1]))

    # Establecer un bitrate máximo
    max_bitrate = 8000  # 8000 kbps (8 Mbps) - puede ajustarse según sea necesario

    # Ajustar el bitrate para que no exceda el bitrate máximo
    bitrate = min(target_bitrate, max_bitrate)

    resized_clip.write_videofile(output_video, codec='libx264', bitrate=f"{bitrate}k", threads=8, fps=clip.fps, preset='ultrafast')

if __name__ == "__main__":
    input_video = r"E:\pruebaxd\Empate Agónico De Mauricio Isla.mp4"  # Ruta al video original
    file_name, file_extension = os.path.splitext(input_video)
    output_video = f"{file_name}_redim1080{file_extension}"  # Nombre del video redimensionado
    target_resolution = (1920, 1080)  # Resolución deseada (1080p)

    resize_video(input_video, output_video, target_resolution)